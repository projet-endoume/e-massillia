    <div class="contact-container">
        <form id="contact" action="index.php?lg=en&uc=contact&action=validEmail" method="post">
            <?php
            if($envoye != null)
            {
                if($envoye == 1)
                {
                    echo "<div style='text-align:center; color: green;font-weight: bold;'>EMAIL ENVOYE</div>";
                }
                else if($envoye == 0)
                {
                    echo "<div style='text-align:center; color: red;font-weight: bold;'>EMAIL NON ENVOYE</div>";
                }
                else if($envoye == 2)
                {
                    echo "<div style='text-align:center; color: red;font-weight: bold;'>CAPTCHA INCORRECT</div>";
                }
            }
            ?>
            <h3>Contact Us</h3>
            <h4>Write a message if you want to interact with us.</h4>
            <fieldset>
                <input name="nom" placeholder="Your name" type="text" tabindex="1" required autofocus >
            </fieldset>
            <fieldset>
                <input name="mail" placeholder="Your email adress" type="email" tabindex="2" required>
            </fieldset>
            <fieldset>
                <select name="objet" tabindex="4" style="width: 106%;">
                    <option value="Article" selected>Article</option>
                    <option value="Information">Information</option>
                    <option value="Question">Question</option>
                    <option value="Bug">Bugs</option>
                    <option value="Map">Map</option>
                    <option value="Schedule">schedule</option>
                </select>
            </fieldset>
            <fieldset>
                <textarea name="message" placeholder="Message..." tabindex="5" required></textarea>
            </fieldset>
            <fieldset>
                <input name="captcha" placeholder="Write 'Im not a robot'." type="text" tabindex="6" required autofocus >
            </fieldset>
            <fieldset class="submit">
                <button name="submit" type="submit" id="contact-submit" data-submit="...Sendind">Send</button>
            </fieldset>
            
        </form>
    </div>