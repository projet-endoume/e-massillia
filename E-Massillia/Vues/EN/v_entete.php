<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>E-Massilia</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <script src="././scripts/script.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link href="././styles/styles.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="./styles/print.css" type="text/css" rel="stylesheet" media="print"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:200" rel="stylesheet">
<!--        <script>
        $(document).ready(function(){
            $('img#btn-map').mouseenter(function(){
                $(this).css({
                    width: '80px',
                    height: '80x'
                });
            });
            $('img#btn-map').mouseout(function(){
                $(this).css({
                    width: '64px',
                    height: '64px'
                });
            });
        });
        </script>-->
    </head>
    <body id="page-top">
        <nav class="navbar-brand full_width" id="sideNav">
            <ul class="navbar-nav">
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=en&uc=home">home</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=en&uc=discover">discover</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <div class="nav-link" href="#page-top">&MediumSpace;</div>
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=en&uc=schedule">schedule</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=en&uc=contact">contact</a>
                    <hr class="m-0">
                </li>
            </ul>
            <div>
                <a href="index.php?lg=fr&uc=accueil" style="text-decoration: none;"><img src="./img/logo.png" alt="logo du site." class="logo-top"></a>
            </div>
            <div>
                <a href="index.php?lg=fr&uc=accueil" style="text-decoration: none;"><img class="langue" src="./img/france.png" alt="changer de langue."></a>
            </div>
        </nav>