<div class="container">
    <div class="map">
        <iframe src="https://www.google.com/maps/d/embed?mid=1uRiCIdKn5IMCHuRiNk9xvzK2L013LsAB" width="640" height="480"></iframe>
    </div>
    <div class="nav-btn">
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('foo');">Hotels</a></button>
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Bar et Restaurant');">Bars & Restaurants</a></button>
        <button class="btn" ><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Site touristique');">Tourist attractions</a></button>
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Activité sportive et site naturel');">Sports activity and natural site</a></button>
    </div>
    <div class="btn-container">
        <div class="image" style="display: none" id="foo">
            <article class="casearticle">
                <div class="casephoto">
                    <img  class="photoCarte" src="././img/map/IMG_1391.JPG"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Hotel Peron :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Real time machine, the hotel Peron will allow you to feel in a very vintage time. If you are looking for a place from which to observe the immensity of the blue sea, this hotel is for you.</p>
                    </div><br><br><br><br><br><br><br><br><br>
                    <div class="cardLink">
                        <a class="myText" href="https://www.google.com/maps/place/H%C3%B4tel+Peron/@43.2877078,5.3501991,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c0d61ecd25f5:0x16e139609dd9f018!8m2!3d43.2877039!4d5.3523878" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/petit_nice.JPG"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Petit Nice – Passedat :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            High place of Marseillaise gastronomy, Petit Nice will offer you a unique experience for fish lovers. Ideal place to invite your companion for a candlelight dinner. A must for those who can afford it.</p>
                    </div><br><br><br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c6d56603a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.3520969" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="image" style="display: none" id="Bar et Restaurant">
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/bistrot_plage.jpg">
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Le Bistrot Plage :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Like on an almost island, the bistro beach is located below the cornice, in front of the valley of Auffes with the Mediterranean Sea for only decoration. From the terraces surrounded by the waves overlooking the islands of Friuli and the castle of If.
                            <br><br>
                            The Bistrot Beach restaurant, private beach, tapas bar. With à la carte, the choice is simple and varied, salads, fish, meat or pizzas. Take a dip and sit on a lounger, we will also serve you. The tapas bar offers a wide selection of nectars and cocktails in a relaxed atmosphere.
                        </p>
                    </div><br><br><br><br>
                    <div class="cardLink"><a href="https://www.google.com/maps/place/Le+Bistrot+Plage+-+Restaurant+Marseille/@43.3149658,5.3567711,13z/data=!4m5!3m4!1s0x12c9c12a1d52f0e1:0x4359ed421e3b2085!8m2!3d43.2861789!4d5.3505961" target="blank"><img src="img/right-arrow.png"/>Map here</a></div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/epuisette.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>L’Epuisette :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Just after the bridge, perched on its rock, the Epuisette offers a breathtaking view of the cove of Marseille. All around, the deep blue Mediterranean Sea offers a constantly renewed spectacle.
                            <br>
                            The comings and goings of the small fishing boats punctuate the days while in the evening, facing the restaurant, a sunset illuminates the room with a light in shades of orange.
                        </p>    
                    </div><br><br><br><br><br><br><br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/L'+Epuisette/@43.2857429,5.3477123,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c12a3bc11c4d:0xf7c1ee194cac01d3!8m2!3d43.285739!4d5.349901" target="blank"><img src="img/right-arrow.png"/>Map here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/fonfon.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Chez Fonfon :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Eating a Bouillabaisse in Marseille is a symbol of Marseilles culture. Bouillabaisse is a specialty of Marseille. It is a fish-based dish that can be enjoyed as a starter (as a broth), or as a main course.
                            <br>
                            Simply put, Fonfon is the specialist of fish and bouillabaisse. Transforming the cuisine of this typical Marseille dish into a work of art.
                        </p>
                    </div><br><br><br><br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Chez+Fonfon/@43.2856227,5.3491134,17z/
                           data=!3m1!4b1!4m5!3m4!1s0x12c9c12a0d2ab5bf:0xcd80733d1e15d3d!8m2!3d43.2856188!4d5.3513021" target="blank"><img src="img/right-arrow.png"/>Map here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/jeannot.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Jeannot :</h2>
                        <p style="text-align: justify; padding-right: 2%;">            
                            Alexandre Pinna welcomes you to his restaurant in the small fishing port of Vallon des Auffes in Marseille.
                            <br>
                            You will be won over by the frame and the wide choice of the map.
                            <br>
                            You will be seduced by the friendliness and the sympathy of its staff.
                            <br>
                            You can taste the specialties "house" in its large room or on the terrace with a breathtaking view of the Vallon des Auffes.
                        </p>
                    </div><br><br><br><br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Pizzeria+Chez+Jeannot/@43.2853718,5.3494782,17z/
                           data=!3m1!4b1!4m5!3m4!1s0x12c9c12a6d873625:0x27c8e9e4db67219d!8m2!3d43.2853679!4d5.3516669" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="img/map/Map_350px/petit_nice.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Petit Nice – Passedat :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            High place of Marseille gastronomy, Petit Nice will offer you a unique experience. Ideal place to invite your companion for a candlelight dinner.
                        </p>
                    </div><br><br><br><br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!
                           3m1!4b1!4m5!3m4!1s0x12c9c6d56603a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.3520969"target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="image-btn">
            <div class="image" style="display: none" id="Site touristique">
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/porte_orient.jpg">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Monument to the Dead of the Army of the East and the heroes of distant lands :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Classified as a historic monument since May 24, 2011, this monument is a real tribute to all those who fell in the East during the First World War.
                            </p>
                        </div><br><br><br><br><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c6d566
                               03a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.
                               3520969"target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/st_eugene.jpg">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Saint Eugene Church of Endoume :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Erected in 1842 by Mr Eugene de Mazenod in the middle of the guinguettes and cabanas this church is hidden in the famous panorama of Endoume. So discreet that we find little information on its history except that it regularly offers masses and concerts such as the festival "Les Rencontres Vocales".
                            </p>
                        </div><br><br><br><br><br><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Eglise+Saint-Eug%C3%A8ne/@43.2843621,5.3502667,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c0d5815
                               92037:0xeb9820eb3db0d1e3!8m2!3d43.2843582!4d5.3524554" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/maregraphe.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Marégraphe of Marseille :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Have you ever wondered from what we calculate the altitude of the mountains in France? The Marégraphe de Marseille is your answer. This little house is indeed the point 0!
                                The tide gauge of Marseille is a tide gauge set up in 1883 at number 174 of the Corniche in Marseille, in Anse Calvo. The goal is to determine an origin of the altitudes for mainland France. The measurements were carried out continuously from January 1, 1884 to December 31, 1896, over 13 years.</p>
                        </div><br><br><br><br><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Le+Mar%C3%A9graphe/@43.2788124,5.3516577,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c72a5bb09955
                               :0x9b0d261ad237f77c!8m2!3d43.2788085!4d5.3538464"target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                
                <h2 class="myText4" style="margin-left: 5%;">Outside the district of Endoume</h2>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/notre_dame.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Our Lady of the Guard :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Notre-Dame-de-la-Garde, often nicknamed "The Good Mother", is a minor basilica of the Roman Catholic Church.
                                <br>
                                It is located in Marseille, on a limestone peak 149 meters high elevated 13 meters thanks to the walls and foundations of an old fort. Notre-Dame-de-la-Garde Hill has been a classified site since 1917.
                                <br>
                                The panorama of the basilica Notre-Dame-de-la-Garde is exceptional, an overview of the city of Marseille, which has spread widely on the west of the sea. Bordered by the hills north of the sea. 'Star, the Nerthe, the chain of Estaque, east of Sainte-Baume, the Garlaban, south Carpiagne, Marseilleveyre and even in good weather, Mont Ventoux.
                                <br>
                                From this vast depression emits an urgent calculation peak of height of 162 meters at the top of the basilica Notre-Dame de la Garde.
                            </p>
                        </div><br><br><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Basilique+Notre-Dame+de+la+Garde/@43.2839572,5.369049,17z/data=!3m1!4b1!4m5!3m4!1s0x1
                               2c9c0c976b89df5:0x89254f95b5ee2ded!8m2!3d43.2839533!4d5.3712377" target="blank"><img src="img/right-arrow.png"/> Map Here</a>
                        </div>
                    </div>
                </article>


            </div>
        </div>
        <div class="image-btn">
            <div class="image" style="display:none" id="Activité sportive et site naturel">
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/vallon_by_night"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Small port of Vallon des Auffes :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Hidden between two cliffs and facing the sea, the small port of Vallon des Auffes offers an authentic and picturesque experience of Marseille at the time of Marcel Pagnol.
                            </p>
                        </div><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Vallon+des+Auffes/@43.2854617,5.3414922,15z/data=!3m1!4b1!4m5!3m4!1s0x12c9c
                               12a386f8f2f:0x84450805ddbf7c7d!8m2!3d43.285447!4d5.350247" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/maldorme">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Cove of Maldormé :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Located on the peninsula of Malmousque and not far from the Anse de la Fausse Monnaie, this pebble beach is protected from the wind to offer you a quiet and relaxing moment of relaxation.
                            </p>
                        </div><br><br><br><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Anse+de+Maldorme,+13007+Marseille/@43.2802418,5.349265,17z/data=!3m1!4b1!4m5
                               !3m4!1s0x12c9c6d568c1f745:0xb6a3d274cac5c1f2!8m2!3d43.2802379!4d5.3514537" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/anse_fausse_monnaie.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Cove and Bridge of the False Mint :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Cradle of the legend of Charles Cazault, the Anse de la Fausse Monnaie is a haven of peace for those who wish to escape the bustle of the great beaches. Will you throw a piece from the top of his deck that one of your wishes is realized?
                            </p>
                        </div><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Anse+de+la+Fausse+Monnaie/@43.2797807,5.3443642,15z/data=!3m1!4b1!4m5!3m4!1s0x12
                               c9c72a8dc2f51f:0x874578edace27bea!8m2!3d43.279766!4d5.353119"target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>
                <h2 class="myText4" style="margin-left: 5%;">Outside the district of Endoume</h2>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/cercle"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Circle of swimmers :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Blend between freshwater pool, Club House and Olympic pool and cabins. The Circle of Swimmers is a sports institution around water sports. Although it is necessary to be a member or a guest to enter their premises, the Circle of swimmers has seen the best French swimmers among them.
                            </p>
                        </div><br><br><br><br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Cercle+des+Nageurs+de+Marseille/@43.2797641,5.3356094,14z/data=!4m5!3m4!1s0x12c9c0da6
                               730c671:0x561abc23cb17f2d9!8m2!3d43.2916536!4d5.3549733" target="blank"><img src="img/right-arrow.png"/>Map Here</a>
                        </div>
                    </div>
                </article>

            </div>
        </div>
    </div>
</div>
