    <div class="contact-container">
        <form id="contact" action="index.php?lg=fr&uc=contact&action=validEmail" method="post">
            <?php
            if($envoye != null)
            {
                if($envoye == 1)
                {
                    echo "<div style='text-align:center; color: green;font-weight: bold;'>EMAIL ENVOYE</div>";
                }
                else if($envoye == 0)
                {
                    echo "<div style='text-align:center; color: red;font-weight: bold;'>EMAIL NON ENVOYE</div>";
                }
                else if($envoye == 2)
                {
                    echo "<div style='text-align:center; color: red;font-weight: bold;'>CAPTCHA NON VALIDE</div>";
                }
            }
            ?>
            <h3> Contactez-nous</h3>
            <h4>Rédigé un message si vous souhaitez intéragir avec nous.</h4>
            <fieldset>
                <input name="nom" placeholder="Votre nom" type="text" tabindex="1" required autofocus >
            </fieldset>
            <fieldset>
                <input name="mail" placeholder="Vôtre adresse email" type="email" tabindex="2" required>
            </fieldset>
            <fieldset>
                <select name="objet" tabindex="4" style="width: 106%;">
                    <option value="article" selected>Article</option>
                    <option value="information">Information</option>
                    <option value="question">Question</option>
                    <option value="bug">Bugs</option>
                    <option value="map">Map</option>
                    <option value="agenda">Agenda</option>
                </select>
            </fieldset>
            <fieldset>
                <textarea name="message" placeholder="Message..." tabindex="5" required></textarea>
            </fieldset>
            <fieldset>
                <input name="captcha" placeholder="Ecrire 'Je ne suis pas un robot'." type="text" tabindex="6" required autofocus >
            </fieldset>
            <fieldset class="submit">
                <button name="submit" type="submit" id="contact-submit" data-submit="...En cours d'envoie">Envoyer</button>
            </fieldset>
            
        </form>
    </div>