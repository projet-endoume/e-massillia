<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>E-Massilia</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <script src="././scripts/script.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link href="././styles/styles.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="./styles/print.css" type="text/css" rel="stylesheet" media="print"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:200" rel="stylesheet">
    </head>
    <body id="page-top">
        <nav class="navbar-brand full_width" id="sideNav">
            <ul class="navbar-nav">
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=fr&uc=accueil">accueil</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=fr&uc=decouvrir">découvrir</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <div class="nav-link" href="#page-top">&MediumSpace;</div>
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=fr&uc=agenda">agenda</a>
                    <hr class="m-0">
                </li>
                <li class="width_3_12" id="carre">
                    <a class="nav-link" href="./index.php?lg=fr&uc=contact">contact</a>
                    <hr class="m-0">
                </li>
            </ul>
            <div>
                <a href="#page-top" style="text-decoration: none;"><img src="./img/logo.png" alt="logo du site." class="logo-top"></a>
            </div>
            <div>
                <a href="index.php?lg=en&uc=home" style="text-decoration: none;"><img class="langue" src="./img/united-kingdom.png" alt="changer de langue." onclick='this.src = this.src === "img/united-kingdom.png" ? "./img/france.png" : "./img/united-kingdom.png";'></a>
            </div>
        </nav>