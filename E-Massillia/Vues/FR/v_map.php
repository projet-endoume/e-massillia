<div class="container">
    <div class="map">
        <iframe src="https://www.google.com/maps/d/embed?mid=1uRiCIdKn5IMCHuRiNk9xvzK2L013LsAB" width="640" height="480"></iframe>
    </div>
    <div class="nav-btn">
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('foo');">Hôtels</a></button>
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Bar et Restaurant');">Bars & Restaurants</a></button>
        <button class="btn" ><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Site touristique');">Sites touristiques</a></button>
        <button class="btn"><a class="fancy-button pop-onhover bg-gradient4" onclick="toggle_visibility('Activité sportive et site naturel');">Activitées sportives & sites naturels</a></button>
    </div>
    <div class="btn-container">
        <div class="image" style="display: none" id="foo">
            <article class="casearticle">
                <div class="casephoto">
                    <img  class="photoCarte" src="././img/map/IMG_1391.JPG"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Hôtel Peron :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Véritable machine à voyager dans le temps, l’hôtel Peron vous permettra de vous sentir dans une époque très vintage. Si vous cherchez un lieu depuis lequel observer l’immensité bleue de la mer, cet hôtel est fait pour vous.</p>
                    </div><br><br><br><br><br><br><br><br><br>
                    <div class="cardLink">
                        <a class="myText" href="https://www.google.com/maps/place/H%C3%B4tel+Peron/@43.2877078,5.3501991,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c0d61ecd25f5:0x16e139609dd9f018!8m2!3d43.2877039!4d5.3523878" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/petit_nice.JPG"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Petit Nice – Passedat :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Haut lieu de la gastronomie Marseillaise, le Petit Nice vous offrira une expérience unique pour les amateurs de poissons. Endroit idéal pour y convier votre compagne pour un dîner aux chandelles. Un endroit incontournable pour celles et ceux qui pourront se le permettre.</p>
                    </div></br></br></br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c6d56603a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.3520969" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="image" style="display: none" id="Bar et Restaurant">
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/bistrot_plage.jpg">
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Le Bistrot Plage :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Comme sur une presque île, le bistrot plage se situe en contrebas de la corniche, en face du vallon des Auffes avec la mer Méditerranée pour seul décor. Depuis les terrasses entourées par les flots avec vue sur les îles du Frioul et du château d’If.
                            <br><br>
                            Le Bistrot Plage restaurant, plage privée, bar à tapas. Avec à la carte, le choix est simple et varié, salades, poissons, viande ou pizzas. Piquez une tête et installez-vous sur un transat, on viendra aussi vous servir. Le bar à tapas vous propose de siroter un vaste choix de nectars et de cocktails dans une ambiance détendue.
                        </p>
                    </div></br></br></br></br>
                    <div class="cardLink"><a href="https://www.google.com/maps/place/Le+Bistrot+Plage+-+Restaurant+Marseille/@43.3149658,5.3567711,13z/data=!4m5!3m4!1s0x12c9c12a1d52f0e1:0x4359ed421e3b2085!8m2!3d43.2861789!4d5.3505961" target="blank"><img src="img/right-arrow.png"/>Map here</a></div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/epuisette.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>L’Epuisette :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Juste après le pont, perché sur son rocher, l’Epuisette offre une vue imprenable sur la anse de Marseille. Tout autour, la mer Méditerranée d’un bleu profond, propose un spectacle sans cesse renouvelé.
                            <br>
                            Le va-et-vient des petits bateaux de pêcheurs rythme les journées tandis que le soir, face au restaurant, un coucher illumine la salle d’une lumière aux tons orangés.
                        </p>
                    </div></br></br></br></br></br></br></br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/L'+Epuisette/@43.2857429,5.3477123,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c12a3bc11c4d:0xf7c1ee194cac01d3!8m2!3d43.285739!4d5.349901" target="blank"><img src="img/right-arrow.png"/>Map here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/fonfon.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Chez Fonfon :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Manger une Bouillabaisse à Marseille c’est tout un symbole de la culture Marseillaise. La Bouillabaisse est une spécialité de Marseille. C’est un plat à base de poissons qu’on peut déguster en entrée (sous forme de bouillon), ou en plat principal.
                            <br>
                            Pour faire simple Fonfon est le spécialiste du poisson et de la bouillabaisse. Transformant la cuisine de ce plat typique de Marseille en œuvre d’art.
                        </p>
                    </div></br></br></br></br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Chez+Fonfon/@43.2856227,5.3491134,17z/
                           data=!3m1!4b1!4m5!3m4!1s0x12c9c12a0d2ab5bf:0xcd80733d1e15d3d!8m2!3d43.2856188!4d5.3513021" target="blank"><img src="img/right-arrow.png"/>Map here</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="././img/map/jeannot.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Jeannot :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Alexandre Pinna vous accueille dans son restaurant dans le petit port de pêche du Vallon des Auffes à Marseille. 
                            <br>
                            Vous serez conquis par le cadre et par le large choix de la carte. 
                            <br>
                            Vous serez séduit par la convivialité et la sympathie de son personnel. 
                            <br>
                            Vous pourrez déguster les spécialités « maison » dans sa grande salle ou sur la terrasse avec une vue imprenable sur le Vallon des Auffes.
                        </p>
                    </div></br></br></br></br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Pizzeria+Chez+Jeannot/@43.2853718,5.3494782,17z/
                           data=!3m1!4b1!4m5!3m4!1s0x12c9c12a6d873625:0x27c8e9e4db67219d!8m2!3d43.2853679!4d5.3516669" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                    </div>
                </div>
            </article>
            <article class="casearticle">
                <div class="casephoto">
                    <img class="photoCarte" src="img/map/Map_350px/petit_nice.jpg"/>
                </div>
                <div class="description">
                    <div class="introduction">
                        <h2>Petit Nice – Passedat :</h2>
                        <p style="text-align: justify; padding-right: 2%;">
                            Haut lieu de la gastronomie marseillaise, le Petit Nice vous offrira une expérience unique. Endroit idéal pour y convier votre compagne pour un dîner aux chandelles. </p>
                    </div></br></br></br></br>
                    <div class="cardLink">
                        <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!
                           3m1!4b1!4m5!3m4!1s0x12c9c6d56603a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.3520969"target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="image-btn">
            <div class="image" style="display: none" id="Site touristique">
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/porte_orient.jpg">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Monument aux Morts de l’Armée d’Orient et aux héros des terres lointaines :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Classé monument historique depuis le 24 mai 2011, ce monument est un véritable hommage à tous ceux qui sont tombés en Orient lors de la première guerre mondiale.
                            </p>
                        </div></br></br></br></br></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Le+Petit+Nice+Passedat/@43.2801231,5.3499082,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c6d566
                               03a0f1:0x45445ec040301421!8m2!3d43.2801192!4d5.
                               3520969"target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/st_eugene.jpg">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Église Saint-Eugène-d’Endoume :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Erigée en 1842 par Mr Eugène de Mazenod au milieu des guinguettes et des cabanons cette église se cache dans le célèbre panorama d'Endoume. Tellement discrète que l'on trouve peu d'informations sur son histoire si ce n'est qu'elle propose régulièrement des messes et des concerts tels que le festival « Les Rencontres Vocales ».  </p>
                        </div></br></br></br></br></br></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Eglise+Saint-Eug%C3%A8ne/@43.2843621,5.3502667,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c0d5815
                               92037:0xeb9820eb3db0d1e3!8m2!3d43.2843582!4d5.3524554" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/maregraphe.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Marégraphe de Marseille :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Vous êtes-vous déjà demandé à partir de quoi nous calculions l’altitude des monts en France ? Le Marégraphe de Marseille est votre réponse. Cette petite maison est en effet le point 0 ! 
                                Le marégraphe de Marseille est un marégraphe mis en place en 1883 au numéro 174 de la Corniche à Marseille, dans l'Anse Calvo. Le but est de déterminer une origine des altitudes pour la France continentale. Les mesures ont été effectuées en continu du 1ᵉʳ janvier 1884 au 31 décembre 1896, sur 13 ans</p>
                        </div></br></br></br></br></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Le+Mar%C3%A9graphe/@43.2788124,5.3516577,17z/data=!3m1!4b1!4m5!3m4!1s0x12c9c72a5bb09955
                               :0x9b0d261ad237f77c!8m2!3d43.2788085!4d5.3538464"target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                
                <H2 class="myText4" style="margin-left: 5%;">Hors quartier de Endoume</H2>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/notre_dame.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Notre Dame de la Garde :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Notre-Dame-de-la-Garde, souvent surnommée « la Bonne Mère », est une des basiliques mineures de l'Église catholique romaine. 
                                <br>
                                Elle est située à Marseille, sur un piton calcaire de 149 mètres d'altitude surélevé de 13 mètres grâce aux murs et soubassements d'un ancien fort. La colline Notre-Dame-de-la-Garde constitue un site classé depuis 1917.
                                <br>
                                Le panorama de la Basilique Notre-Dame-de-la-Garde est exceptionnel, une vue d'ensemble sur la ville de Marseille, qui s'ouvre largement à l'ouest sur la mer. Bordé par des collines au nord de l'Étoile, la Nerthe, la chaîne de l'Estaque, à l'est la Sainte-Baume, le Garlaban, au sud Carpiagne, Marseilleveyre et même par très beau temps, le Mont Ventoux. 
                                <br>
                                De cette vaste dépression émerge un piton de calcaire urgonien d’une hauteur de 162 mètres au sommet duquel s'élève la basilique Notre-Dame de la Garde.
                            </p>
                        </div></br></br></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Basilique+Notre-Dame+de+la+Garde/@43.2839572,5.369049,17z/data=!3m1!4b1!4m5!3m4!1s0x1
                               2c9c0c976b89df5:0x89254f95b5ee2ded!8m2!3d43.2839533!4d5.3712377" target="blank"><img src="img/right-arrow.png"/> Y Aller</a>
                        </div>
                    </div>
                </article>


            </div>
        </div>
        <div class="image-btn">
            <div class="image" style="display:none" id="Activité sportive et site naturel">
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/vallon_by_night"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Petit port du Vallon des Auffes :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Caché entre deux falaises et tourné vers la mer, le petit port du Vallon des Auffes offre une expérience authentique et pittoresque du Marseille à l’époque de Marcel Pagnol. </p>
                        </div></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Vallon+des+Auffes/@43.2854617,5.3414922,15z/data=!3m1!4b1!4m5!3m4!1s0x12c9c
                               12a386f8f2f:0x84450805ddbf7c7d!8m2!3d43.285447!4d5.350247" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/maldorme">
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Anse de Maldormé :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Située sur la presqu’île de Malmousque et non loin de l’anse de la Fausse Monnaie, cette plage de galets est protégée du vent pour vous offrir un moment de détente calme et relaxant. </p>
                        </div></br></br></br></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Anse+de+Maldorme,+13007+Marseille/@43.2802418,5.349265,17z/data=!3m1!4b1!4m5
                               !3m4!1s0x12c9c6d568c1f745:0xb6a3d274cac5c1f2!8m2!3d43.2802379!4d5.3514537" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/anse_fausse_monnaie.jpg"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Anse et pont de la Fausse Monnaie :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Berceau de la légende de Charles Cazault, l’Anse de la Fausse Monnaie est un havre de paix pour ceux qui désirent fuir l’agitation des grandes plages. Jetterez-vous une pièce du haut de son pont que l’un de vos vœux soit réalisé ?</p>
                        </div></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Anse+de+la+Fausse+Monnaie/@43.2797807,5.3443642,15z/data=!3m1!4b1!4m5!3m4!1s0x12
                               c9c72a8dc2f51f:0x874578edace27bea!8m2!3d43.279766!4d5.353119"target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>
                <h2 class="myText4" style="margin-left: 5%;">Hors quartier de Endoume</h2>
                <article class="casearticle">
                    <div class="casephoto">
                        <img class="photoCarte" src="img/map/Map_350px/cercle"/>
                    </div>
                    <div class="description">
                        <div class="introduction">
                            <h2>Cercle des nageurs :</h2>
                            <p style="text-align: justify; padding-right: 2%;">
                                Mélange entre bassin d'eau douce, Club House puis bassin olympique et cabines. Le Cercle des nageurs est une institution sportive autour des sports aquatiques. Bien qu'il soit nécessaire d'être un membre ou un invité pour pénétrer dans leurs locaux, le Cercle des nageurs a vu en son sein les plus grands nageurs français.</p>
                        </div></br></br></br></br>
                        <div class="cardLink">
                            <a href="https://www.google.com/maps/place/Cercle+des+Nageurs+de+Marseille/@43.2797641,5.3356094,14z/data=!4m5!3m4!1s0x12c9c0da6
                               730c671:0x561abc23cb17f2d9!8m2!3d43.2916536!4d5.3549733" target="blank"><img src="img/right-arrow.png"/>Y Aller</a>
                        </div>
                    </div>
                </article>

            </div>
        </div>
    </div>
</div>
