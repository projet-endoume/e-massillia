        <div class="full_width footeur footeur_bg">
            <img class= "logo-footer" src="./img/logo.png" alt="logo" style="width: 150px; position:absolute; left:0px;">
            <div class="footeur_up width_3_12">
                <h3 style="font-variant: montserrat; font-size : 22px;"> Plan du site</h3>
                <span id="item-footeur"><a href="././index.php?lg=fr&uc=decouvrir">Découvrir</a></span><br>
                <span id="item-footeur"><a href="././index.php?lg=fr&uc=agenda">Agenda</a></span><br>
                <span id="item-footeur"><a href="././index.php?lg=fr&uc=contact">Contact</a></span><br>
                <span id="item-footeur"><a href="././index.php?lg=fr&uc=map">Carte d'Endoume</a></span><br>
            </div>
            <div class="footeur_up width_3_12" id="footer1">
                <h3 style="font-variant: montserrat; font-size : 22px;">Nous suivre</h3>
                <span class="icon" id="item-footeur"><a href="https://fr-fr.facebook.com/" target="_blank"><img class="icon" src="./img/facebook.png" alt="facebook" height="32" width="32"></a></span>
                <span class="icon"  id="item-footeur"><a href="https://www.instagram.com/?hl=fr" target="_blank"><img class="icon" src="./img/instagram.png" alt="instagram" height="32" width="32"></a></span>
                <span class="icon" id="item-footeur"><a href="https://www.snapchat.com/l/fr-fr/" target="_blank"><img class="icon" src="./img/snapchat.png" alt="snapchat" height="32" width="32"></a></span>
            </div>
            <div class="full_width" style="bottom: 0;margin-top: 14%;margin-bottom: -5%;">
                <hr class="m-0" style="opacity: 0.5;">
                <hr class="m-0" style="opacity: 0.5;">
                <br>
                <p style="text-align: center;bottom: 0;font-size: 10px; color: #888;"><i>Ce site à été réalisé par <b>E-Massilia</b> &copy; Tous droits réservés.</i> <a href="../../../../../sources/back-office/Back-Office/index.php?uc=connexion" target="_blank" style="text-decoration: none; color:#FF8000">Gérer le site</a>.</p>
            </div>
        </div>
    </body>
</html> 