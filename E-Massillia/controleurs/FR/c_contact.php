<?php
require_once ("include/class.pdoMassilia.inc.php");
if(!isset($_REQUEST['action'])){
	$_REQUEST['action'] = 'contact';
}
$action = $_REQUEST['action'];
$pdo = PdoMassilia::getPdoMassilia();
$envoye = null;
$secure = "";
switch ($action) 
{
    case 'contact':
    {
        include("vues/FR/v_contact.php");
        break;
    }
    case 'validEmail': 
    {
        $nom = $_POST['nom'];
        $objet = $_POST['objet'];
        $mail = $_POST['mail'];
        $message = $_POST['message'];
        $secure = $_POST['captcha'];
        
        if($secure == "Je ne suis pas un robot")
        {
            if ($pdo->insertEmail($mail, $objet, $message, $nom) == 1) 
            {
                $envoye = 1;
            } 
            else 
            {
                $envoye = 0;
            }
        }
        else 
        {
            $envoye = 2;
        }
        include("vues/FR/v_contact.php");
        break;
    }
}
?>
