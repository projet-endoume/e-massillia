﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application E-Massilia
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoMassilia qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Ludovic Genevois
 * @version    1.0
 */

class PdoMassilia{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=e-massilia';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoMassilia=null;
                
//        private static $serveur='mysql:host=db764683487.hosting-data.io';
//      	private static $bdd='dbname=db764683487';   		
//      	private static $user='dbo764683487' ;    		
//      	private static $mdp='E-massilia13' ;	
//		private static $monPdo;
//		private static $monPdoMassilia=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct(){
    	PdoMassilia::$monPdo = new PDO(PdoMassilia::$serveur.';'.PdoMassilia::$bdd, PdoMassilia::$user, PdoMassilia::$mdp, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); 
		PdoMassilia::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoMassilia::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 
 * Appel : $instancePdoMassilia = PdoMassilia::getPdoMassilia();
 
 * @return l'unique objet de la classe PdoMassilia
 */
	public  static function getPdoMassilia(){
		if(PdoMassilia::$monPdoMassilia==null){
			PdoMassilia::$monPdoMassilia= new PdoMassilia();
		}
		return PdoMassilia::$monPdoMassilia;  
	}
        
        
        public function insertEmail($mail, $objet, $message, $nom)
        {
            $req = "INSERT INTO email(EMAIL_ADRESSE_EMAIL, EMAIL_TITRE_EMAIL, CONTENU_EMAIL, NOM, isCloture) VALUES ('$mail', '$objet', '$message', '$nom', 0);";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result == 1){
                return 1;
            }else{
                return 0;
            }
        }
        
        public function getArticlesFR()
        {
            $req = "SELECT PUB_CONTENU_FR FROM publication";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes;
        }

        function getArticlesEN()
        {
            $req = "SELECT PUB_CONTENU_EN FROM publication";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes;
        }
}
?>