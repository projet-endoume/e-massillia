<?php
require './include/class.pdomassilia.inc.php';

$pdo = PdoMassilia::getPdoMassilia();

if(!isset($_REQUEST['lg'])){
    $_REQUEST['lg'] = 'fr';
}
$lg = $_REQUEST['lg'];

if(!isset($_REQUEST['uc'])){
    $_REQUEST['uc'] = 'accueil';
}
$uc = $_REQUEST['uc'];

switch ($lg)
{
    
    case 'fr':
    {
        include_once 'vues/FR/v_entete.php';
        switch($uc){
            case 'accueil':{
                include("vues/FR/v_accueil.php");break;
            }

            case 'decouvrir':{
                $articlesFR = $pdo->getArticlesFR();
                include("vues/FR/v_decouvrir.php");break;
            }

            case 'agenda':{
                include("vues/FR/v_agenda.php");break;
            }

            case 'contact':{
                include("controleurs/FR/c_contact.php");break;
            }
            case 'map':{
                include ("vues/FR/v_map.php");break;
            }
        }
        include ("vues/FR/v_pied.php");
        break;
    }
    
    case 'en':
    {
        include ("vues/EN/v_entete.php");
        switch($uc){
            case 'home':{
                include("vues/EN/v_accueil.php");break;
            }

            case 'discover':{
                $articlesFR = $pdo->getArticlesEN();
                include("vues/EN/v_decouvrir.php");break;
            }

            case 'schedule':{
                include("vues/EN/v_agenda.php");break;
            }

            case 'contact':{
                include("controleurs/EN/c_contact.php");break;
            }
            case 'map':{
                include ("vues/EN/v_map.php");break;
            }
        }
        include ("vues/EN/v_pied.php");
        break;
    }
}