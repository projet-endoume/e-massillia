/* 
    Created on : 31 oct. 2018, 18:03:12
    Author     : Ludovic GENEVOIS
*/
function toggle_visibility(id) {
    var e = document.getElementById(id);
    var i = document.getElementsByClassName('image');
    for(t = 0; t < i.length; t++){
        if(i[t]!= e){
            i[t].style.display = 'none';
        }
    }
    e.style.display = ((e.style.display!='none') ? 'none' : 'block');
}

(function($){

    /* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#header__icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    /* Je veux pouvoir masquer le menu si on clique sur le cache */
    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
    })

})(jQuery);